package main

import (
	"math/big"
	"regexp"
)

var (
	netTCPPortRE = regexp.MustCompile(`\d: [0]{8,}:([\w+]{4})`)
)

type Int31Slice []int32

func (x Int31Slice) Len() int           { return len(x) }
func (x Int31Slice) Less(i, j int) bool { return x[i] < x[j] }
func (x Int31Slice) Swap(i, j int)      { x[i], x[j] = x[j], x[i] }

func ParseNetTCP(body string) []int32 {
	var ports []int32
	matches := netTCPPortRE.FindAllStringSubmatch(body, -1)
	for _, match := range matches {
		if len(match) < 2 {
			continue
		}
		n := new(big.Int)
		if i, ok := n.SetString(match[1], 16); ok {
			p := i.Int64()
			ports = append(ports, int32(p))
		}
	}
	return ports
}

func UniqueInt32(intSlice []int32) []int32 {
	var list []int32
	keys := make(map[int32]bool)
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

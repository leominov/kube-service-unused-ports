# kube-service-unused-ports

Поиск несоответствий портов сервисов с открытыми портами подов.

## Использование

```
Usage of ./kube-service-unused-ports:
  -label-selector string
    	Label selector for services (default "project=platform")
  -n string
    	Filter by namespace
```

## Пример отчета

```
INFO[0000] Ingress list: 91
INFO[0000] Service list: 67
WARN[0001] Unknown app-bff-offers/bff-offers ports: [5000], opened: [4444 8080]
ERRO[0001] Unknown app-bff-offers/bff-offers port 5000 used in bff-offers-grpc ingress
WARN[0002] Unknown app-bff-self-service-form-app/bff-self-service-form-app ports: [4000], opened: [4444 5000 5001 9901]
WARN[0002] Unknown app-communications-gateway/communications-gateway ports: [4000], opened: [4444 5000]
WARN[0003] Unknown app-crm-batch-loader/crm-batch-loader ports: [5000], opened: [4000 4444]
WARN[0006] Unknown app-crm-mail-to-leads-svs/crm-mail-to-leads-svs ports: [4000 5000], opened: [4444 8080]
WARN[0007] Unknown app-crm-task-manager/crm-task-manager ports: [4000], opened: [3000 4444 5000]
ERRO[0007] Unknown app-crm-task-manager/crm-task-manager port 4000 used in crm-task-manager-http ingress
ERRO[0009] Failed to get app-finance-analytics/finance-analytics ports: unable to upgrade connection: container not found ("payout-reporting"):
WARN[0009] Unknown app-finance-balance/finance-balance ports: [4000], opened: [4444 5000]
WARN[0010] Unknown app-finance-balance/finance-balance-old ports: [4000], opened: [4444 5000]
WARN[0010] Unknown app-finance-cryptopro/finance-cryptopro ports: [4444], opened: [8080 34951]
WARN[0011] Unknown app-finance-invoice-gateway/finance-invoice-gateway ports: [4000], opened: [4444 5000]
ERRO[0011] Unknown app-finance-invoice-gateway/finance-invoice-gateway port 4000 used in finance-invoice-gateway-http ingress
ERRO[0011] Failed to find app-finance-invoice-svs/finance-invoice-svs pod: no pods found
WARN[0012] Unknown app-finance-invoice/finance-invoice ports: [4000], opened: [4444 5000 9876]
ERRO[0012] Unknown app-finance-invoice/finance-invoice port 4000 used in finance-invoice-http ingress
WARN[0012] Unknown app-finance-receipts-svc/finance-receipts-svc ports: [4000], opened: [4444 5000]
ERRO[0012] Unknown app-finance-receipts-svc/finance-receipts-svc port 4000 used in finance-receipts-svc-http ingress
WARN[0013] Unknown app-finance-recurrent/finance-recurrent ports: [4000], opened: [4444 5000]
WARN[0014] Unknown app-image-processor/image-processor ports: [3000], opened: [4444 5000]
WARN[0015] Unknown app-loyalty-bonus-svs/loyalty-bonus-svs ports: [4000], opened: [4444 5000]
WARN[0016] Unknown app-loyalty-promocodes-svs/loyalty-promocodes-svs ports: [4000], opened: [4444 5000]
WARN[0022] Unknown app-sms-sender/sms-sender ports: [4000], opened: [4444 5000]
WARN[0025] Unknown app-storage-manager/storage-manager ports: [4000], opened: [4444 5000]
ERRO[0025] Unknown app-storage-manager/storage-manager port 4000 used in storage-manager-http ingress
WARN[0025] Unknown app-storage-syncer/storage-syncer ports: [4000], opened: [4444 5000]
ERRO[0025] Unknown app-storage-syncer/storage-syncer port 4000 used in storage-syncer-http ingress
WARN[0026] Unknown app-warehouse-worker-bff/warehouse-worker-bff ports: [4000], opened: [4444 5000]
WARN[0029] Unknown app-web-widget-features/web-widget-features ports: [5000], opened: [4000 4444]
WARN[0030] Unknown app-web-widget-footer/web-widget-footer ports: [5000], opened: [4000 4444]
WARN[0030] Unknown app-web-widget-form/web-widget-form ports: [5000], opened: [4000 4444]
WARN[0032] Unknown app-web-widget-mosaic/web-widget-mosaic ports: [5000], opened: [4000 4444]
WARN[0033] Unknown app-web-widget-navbar/web-widget-navbar ports: [5000], opened: [4000 4444]
WARN[0036] Unknown app-web-widget-texts/web-widget-texts ports: [5000], opened: [4000 4444]
```

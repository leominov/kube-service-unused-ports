package main

import (
	"bufio"
	"bytes"
	"context"
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
)

var (
	serviceLabelSelector = flag.String("label-selector", "project=platform", "Label selector for services")
	namespaceSelector    = flag.String("n", "", "Filter by namespace")
)

func realMain() error {
	flag.Parse()
	client, config, err := NewClientSet()
	if err != nil {
		return err
	}
	ingressList, err := client.NetworkingV1().Ingresses(*namespaceSelector).List(context.Background(), metav1.ListOptions{
		LabelSelector: *serviceLabelSelector,
	})
	if err != nil {
		return err
	}
	logrus.Infof("Ingress list: %d", len(ingressList.Items))
	serviceList, err := client.CoreV1().Services(*namespaceSelector).List(context.Background(), metav1.ListOptions{
		LabelSelector: *serviceLabelSelector,
	})
	if err != nil {
		return err
	}
	logrus.Infof("Service list: %d", len(serviceList.Items))
	for _, service := range serviceList.Items {
		servicePorts := getServicePorts(service)
		pod, err := findPodBySelector(client, service.Namespace, service.Spec.Selector)
		if err != nil {
			logrus.Errorf("Failed to find %s/%s pod: %v", service.Namespace, service.Name, err)
			continue
		}
		podPorts, err := listPodOpenPorts(client, config, pod)
		if err != nil {
			logrus.Errorf("Failed to get %s/%s ports: %v", service.Namespace, service.Name, err)
			continue
		}
		unknownPorts := diffPorts(servicePorts, podPorts)
		if len(unknownPorts) == 0 {
			continue
		}
		logrus.Warnf("Unknown %s/%s ports: %v, opened: %v", service.Namespace, service.Name, unknownPorts, podPorts)
		for _, p := range unknownPorts {
			ingress := findIngressByServiceAndPort(ingressList, service, p)
			if ingress == nil {
				continue
			}
			logrus.Errorf("Unknown %s/%s port %d used in %s ingress", service.Namespace, service.Name, p, ingress.Name)
		}
	}
	return nil
}

func findIngressByServiceAndPort(list *networkingv1.IngressList, service corev1.Service, port int32) *networkingv1.Ingress {
	portName := portName(service, port)
	if portName == "" {
		return nil
	}
	for _, ingress := range list.Items {
		if ingress.Namespace != service.Namespace {
			continue
		}
		for _, rule := range ingress.Spec.Rules {
			for _, p := range rule.HTTP.Paths {
				if p.Backend.Service.Name == service.Name && p.Backend.Service.Port.Name == portName {
					return &ingress
				}
			}
		}
	}
	return nil
}

func portName(service corev1.Service, port int32) string {
	for _, p := range service.Spec.Ports {
		if p.Port == port {
			return p.Name
		}
	}
	return ""
}

func diffPorts(servicePorts, podPorts []int32) []int32 {
	var ports []int32
	for _, sp := range servicePorts {
		var match bool
		for _, pp := range podPorts {
			if sp == pp {
				match = true
			}
		}
		if !match {
			ports = append(ports, sp)
		}
	}
	return ports
}

func getContainerNames(pod *corev1.Pod) []string {
	var names []string
	for _, container := range pod.Spec.Containers {
		names = append(names, container.Name)
	}
	return names
}

func listPodOpenPorts(client kubernetes.Interface, config *rest.Config, pod *corev1.Pod) ([]int32, error) {
	var ports []int32
	for _, c := range getContainerNames(pod) {
		p, err := listPodContainerOpenPorts(client, config, pod, c)
		if err != nil {
			return nil, err
		}
		ports = append(ports, p...)
	}
	ports = UniqueInt32(ports)
	sort.Sort(Int31Slice(ports))
	return ports, nil
}

func listPodContainerOpenPorts(client kubernetes.Interface, config *rest.Config, pod *corev1.Pod, container string) ([]int32, error) {
	cmd := []string{
		"sh",
		"-c",
		"cat /proc/net/tcp*",
	}

	revokeReq := client.CoreV1().RESTClient().
		Post().Resource("pods").
		Name(pod.Name).Namespace(pod.Namespace).
		SubResource("exec").
		VersionedParams(&corev1.PodExecOptions{
			Container: container,
			Command:   cmd,
			Stdin:     true,
			Stderr:    true,
			Stdout:    true,
		}, scheme.ParameterCodec)
	exec, err := remotecommand.NewSPDYExecutor(config, "POST", revokeReq.URL())
	if err != nil {
		return nil, err
	}
	var (
		stderr bytes.Buffer
		stdout bytes.Buffer
	)
	err = exec.Stream(remotecommand.StreamOptions{
		Stderr: bufio.NewWriter(&stderr),
		Stdin:  os.Stdin,
		Stdout: bufio.NewWriter(&stdout),
	})
	if err != nil {
		return nil, fmt.Errorf("%v: %s", err, stderr.String())
	}
	return ParseNetTCP(stdout.String()), nil
}

func findPodBySelector(client kubernetes.Interface, namespace string, selector map[string]string) (*corev1.Pod, error) {
	var labelSelector []string
	for k, v := range selector {
		labelSelector = append(labelSelector, fmt.Sprintf("%s=%s", k, v))
	}
	podList, err := client.CoreV1().Pods(namespace).List(context.Background(), metav1.ListOptions{
		LabelSelector: strings.Join(labelSelector, ","),
		Limit:         1,
	})
	if err != nil {
		return nil, err
	}
	if len(podList.Items) == 0 {
		return nil, fmt.Errorf("no pods found")
	}
	firstPod := podList.Items[0]
	return &firstPod, nil
}

func getServicePorts(service corev1.Service) []int32 {
	var ports []int32
	for _, port := range service.Spec.Ports {
		ports = append(ports, port.Port)
	}
	sort.Sort(Int31Slice(ports))
	return ports
}

func main() {
	err := realMain()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
